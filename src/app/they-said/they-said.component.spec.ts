import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TheySaidComponent } from './they-said.component';

describe('TheySaidComponent', () => {
  let component: TheySaidComponent;
  let fixture: ComponentFixture<TheySaidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TheySaidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TheySaidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
