import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// import {NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarCanvasComponent } from './navbar-canvas/navbar-canvas.component';
import { BannerComponent } from './banner/banner.component';
import { LiveComponent } from './live/live.component';
import { BrandComponent } from './brand/brand.component';
import { FeaturesComponent } from './features/features.component';
import { ProductComponent } from './product/product.component';
import { TheySaidComponent } from './they-said/they-said.component';
import { SubscribeComponent } from './subscribe/subscribe.component';
import { DownloadComponent } from './download/download.component';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarCanvasComponent,
    BannerComponent,
    LiveComponent,
    BrandComponent,
    FeaturesComponent,
    ProductComponent,
    TheySaidComponent,
    SubscribeComponent,
    DownloadComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
