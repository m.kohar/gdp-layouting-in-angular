import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-navbar-canvas',
  templateUrl: './navbar-canvas.component.html',
  styleUrls: ['./navbar-canvas.component.css']
})
export class NavbarCanvasComponent implements OnInit {

   public isCollapsed = true;
   closeResult: string;

   overlay:string = 'none';


   constructor(private modalService: NgbModal) { }

   ngOnInit() {
   }

   modalLogin(content) {
      this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', centered: true}).result.then((result) => {
         this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
         this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
   }

   private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
        return 'by pressing ESC';
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return 'by clicking on a backdrop';
      } else {
        return  `with: ${reason}`;
      }
   }

   overlayOn() {
      this.overlay = 'block';
   }
   overlayOff() {
      this.overlay = 'none';
   }

}
